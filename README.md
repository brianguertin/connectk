# ConnectK

This is a simple game similar to Connect Four except you can customize the width and height of the board, as well as the number of peices in a row needed to win.

I made this for a coding challenge. It took about 2 hours and 15 minutes. There is a ![timelapse video available](https://github.com/brianguertin/connectk/releases/download/0.1/Timelapse.mp4) of the whole process attached to the GitHub release.

### Running

Open the project in IntelliJ and click Run

Or, download ![ConnectK.jar](https://github.com/brianguertin/connectk/releases/download/0.1/ConnectK.jar) from the GitHub release and run:
`java -jar ConnectK.jar`

You can also customize the board size and the number of pieces in a row needed to win:
`java -jar ConnectK.jar width=4 height=4 k=2`


### Screenshots
```
Get 4 in a row to win!

 - - - - - - - 
 - - - - - - - 
 - - - - - - - 
 - - - - - - - 
 - - - - - - - 
 - - - - - - - 

It's red's turn. Enter a column number to drop a piece: 
```

```
 - - - - - - - 
 - - - - - - - 
 - - - - - R - 
 - - - B R B - 
 - - - R B R - 
 R - R R B B B 

The red player wins!!
```
