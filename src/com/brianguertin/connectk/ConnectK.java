package com.brianguertin.connectk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConnectK {

    public static enum Piece {
        RED,
        BLACK
    }

    public static enum MoveResult {
        NEXT_TURN,
        VICTORY,
        STALEMATE
    }

    private static interface Pattern {
        int next(int current);
    }

    private static class IllegalMoveException extends Throwable {
    }

    private final int width, height, k;

    // Coordinates start at bottom left corner (0,0 is bottom left cell)
    private final Piece[][] board;

    public ConnectK(int width, int height, int k) {
        this.width = width;
        this.height = height;
        this.k = k;
        this.board = new Piece[width][height];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getK() {
        return k;
    }

    public Piece getCell(int x, int y) {
        return board[x][y];
    }

    public MoveResult move(Piece piece, int column) throws IllegalMoveException {
        final int x = column - 1; // Player columns are 1-indexed, board coordinates are zero-indexed

        if (x < 0 || x >= width) {
            throw new IllegalMoveException();
        }

        for (int y = 0; y < height; ++y) {
            if (board[x][y] == null) {
                board[x][y] = piece;

                // Horizontal
                if (checkPattern(
                        piece, x - k, y,
                        new Pattern() {
                            @Override
                            public int next(int current) {
                                return current + 1;
                            }
                        }, new Pattern() {
                            @Override
                            public int next(int current) {
                                return current;
                            }
                        })) {
                    return MoveResult.VICTORY;
                }

                // Vertical
                if (checkPattern(
                        piece, x, y - k,
                        new Pattern() {
                            @Override
                            public int next(int current) {
                                return current;
                            }
                        }, new Pattern() {
                            @Override
                            public int next(int current) {
                                return current + 1;
                            }
                        })) {
                    return MoveResult.VICTORY;
                }

                // Northeast diagonal
                if (checkPattern(
                        piece, x - k, y - k,
                        new Pattern() {
                            @Override
                            public int next(int current) {
                                return current + 1;
                            }
                        }, new Pattern() {
                            @Override
                            public int next(int current) {
                                return current + 1;
                            }
                        })) {
                    return MoveResult.VICTORY;
                }

                // Southeast diagonal
                if (checkPattern(
                        piece, x + k, y - k,
                        new Pattern() {
                            @Override
                            public int next(int current) {
                                return current - 1;
                            }
                        }, new Pattern() {
                            @Override
                            public int next(int current) {
                                return current + 1;
                            }
                        })) {
                    return MoveResult.VICTORY;
                }

                if (isBoardFull()) {
                    return MoveResult.STALEMATE;
                }

                return MoveResult.NEXT_TURN;
            }
        }

        throw new IllegalMoveException();
    }

    private boolean isBoardFull() {
        for (int x = 0; x < width; ++x) {
            if (board[x][height - 1] == null) {
                return false;
            }
        }
        return true;
    }

    private boolean checkPattern(Piece newPiece, int x, int y, Pattern xPattern, Pattern yPattern) {
        int numInARow = 0;
        final int maxCellsToSearch = k * 2 - 1;
        for (int i = 0; i < maxCellsToSearch; ++i) {
            x = xPattern.next(x);
            y = yPattern.next(y);
            if (x >= 0 && y >= 0 && x < width && y < height && board[x][y] == newPiece) {
                ++numInARow;
                if (numInARow == k) {
                    return true;
                }
            } else {
                numInARow = 0;
            }
        }
        return false;
    }

    private static void printBoard(ConnectK board) {
        for (int y = board.getHeight() - 1; y >= 0; --y) {
            System.out.print(" ");
            for (int x = 0; x < board.getWidth(); ++x) {
                final Piece piece = board.getCell(x, y);
                final String rep;
                if (piece == Piece.RED) {
                    rep = "R ";
                } else if (piece == Piece.BLACK) {
                    rep = "B ";
                } else {
                    rep = "- ";
                }
                System.out.print(rep);
            }
            System.out.println();
        }
        System.out.flush();
    }

    private static void printUsage(String badArgument, String cmdName) {
        System.out.println("Unrecognized argument: " + badArgument);
        System.out.println("Usage: " + cmdName + " width=[number>0] height=[number>0] k=[number>0]");
    }

    public static void main(String[] args) throws IOException {
        final ConnectK connectK;
        {
            int width = 7, height = 6, k = 4;
            for (String arg : args) {
                String[] parts = arg.split("=");
                if (parts.length == 2) {
                    final int value;
                    try {
                        value = Integer.parseInt(parts[1]);
                    } catch (NumberFormatException e) {
                        printUsage(arg, args[0]);
                        return;
                    }

                    if (value < 0) {
                        printUsage(arg, args[0]);
                        return;
                    }

                    if ("width".equals(parts[0])) {
                        width = value;

                    } else if ("height".equals(parts[0])) {
                        height = value;

                    } else if ("k".equals(parts[0])) {
                        k = value;

                    } else {
                        printUsage(arg, args[0]);
                        return;
                    }
                } else {
                    printUsage(arg, args[0]);
                    return;
                }
            }
            connectK = new ConnectK(width, height, k);
        }

        System.out.println("Get " + connectK.getK() + " in a row to win!");

        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ConnectK.Piece nextPiece = ConnectK.Piece.RED;
        boolean gameInProgress = true;
        while (gameInProgress) {
            System.out.println();
            printBoard(connectK);
            System.out.println();
            System.out.print("It's " + nextPiece.toString().toLowerCase() + "'s turn. Enter a column number to drop a piece: ");
            System.out.flush();

            final int column;
            try {
                column = Integer.parseInt(reader.readLine());
            } catch (NumberFormatException e) {
                System.out.println();
                System.out.println("You didn't enter a number!");
                continue;
            }

            final ConnectK.MoveResult result;
            try {
                result = connectK.move(nextPiece, column);
            } catch (IllegalMoveException e) {
                System.out.println();
                System.out.println("That's not a valid move!");
                continue;
            }

            switch (result) {
                case NEXT_TURN: {
                    nextPiece = nextPiece == ConnectK.Piece.RED ? ConnectK.Piece.BLACK : ConnectK.Piece.RED;
                    break;
                }
                case VICTORY: {
                    System.out.println();
                    printBoard(connectK);
                    System.out.println();
                    System.out.println("The " + nextPiece.toString().toLowerCase() + " player wins!!");
                    gameInProgress = false;
                    break;
                }
                case STALEMATE: {
                    System.out.println();
                    printBoard(connectK);
                    System.out.println();
                    System.out.println("No more moves. I guess it's a tie.");
                    gameInProgress = false;
                    break;
                }
            }
        }
    }
}
